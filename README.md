# RP Cheker

- A simple tool to check if a video file corrupted by calculate PSNR/SSIM with another video file.

    Feature
--------------------
-    Checking video files by some simple steps.
-    Show overall PSNR/SSIM in one chart,

    Install
--------------------
-    You must have .NET Framework 4.6 available from Windows Update.
-    You must have [VapourSynth R29](https://github.com/vapoursynth/vapoursynth/releases) or higher with [mvsfunc](https://github.com/HomeOfVapourSynthEvolution/mvsfunc/releases) and [python 3.5.0](https://www.python.org/downloads/) or higher.
-    You must have [FFmpeg](https://ffmpeg.org/) for SSIM support.

    Directions
--------------------

    Thanks to
--------------------
-    vpy script https://www.nmm-hd.org/newbbs/viewtopic.php?f=23&t=1813
-    EFS for improvement of adaptive resolution compare.
-    mvsfunc https://github.com/HomeOfVapourSynthEvolution/mvsfunc


    Source Code
--------------------
-    https://bitbucket.org/TautCony/rp-checker
